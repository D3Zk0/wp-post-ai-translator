<?php


return [
    "API_KEY" => "",
    "OPEN_AI_URL" => "https://api.openai.com/v1/chat/completions",
    "MAX_TOKENS" => 20,
    "MODEL" => "gpt-3.5-turbo",
];
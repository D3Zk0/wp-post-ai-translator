## AI WORDPRESS POSTS TRANSLATIONS

Translates the given content from the specified input language to the specified output language using the provided API key.

## Installation

To install the `d3x/ai_translator` library, follow these steps:

1. Make sure you have [Composer](https://getcomposer.org/) installed on your system.

2. Open a terminal or command prompt and navigate to your project directory.

3. Run the following Composer command to add the library to your project:

```bash
composer require d3x/ai_translator
```

### Parameters
- `$api_key` (string): The OPENAI API key for accessing the translation service.
- `$content` (string): The content to be translated.
- `$in_language` (string, optional): The input language. Default is "slovene".
- `$out_language` (string, optional): The output language. Default is "english".


### Example Usage
```php
require_once 'vendor/autoload.php';
use D3x\AiTranslator\API\Translator;

// Set your OpenAI API key 
$api_key = "YOUR_OPENAI_API_KEY"; 

// The content must be a JSON object 
$content = '{
   "post_title":"This is post title!",
   "post_content":"This is post <strong>content!</strong>",
   "post_excerpt":"This is excerpt"
}'; 

// Set the input language for better results (default language is Slovene) 
$in_language = "english"; 

// Set the output language for better results (default language is English) 
$out_language = "slovene"; 

$response = Translator::translate($api_key, $content, $in_language, $out_language);
```
### Response Example

```php
// Response is returned as php associative array
$response = [
    "post_title" => "To je naslov objave!",
    "post_content" => "To je vsebina objave <strong> vsebina! </strong>",
    "post_excerpt" => "To je odlomek"
];
```
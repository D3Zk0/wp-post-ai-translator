<?php

namespace D3x\AiTranslator;

class Config
{
    private static $configs = [];
    private static $initialized = false;

    public static function initialize($dir = null)
    {
        if (!$dir)
            $dir = dirname(__DIR__)."/config";
        if (self::$initialized) {
            return;
        }
        self::load($dir);
        self::$initialized = true;
    }

    public static function load($dir)
    {
        foreach (glob($dir . '/*.php') as $file) {
            self::$configs[pathinfo($file, PATHINFO_FILENAME)] = require($file);
        }
    }

    public static function get($key)
    {
        self::initialize();

        list($file, $attribute) = explode('.', $key, 2);

        if (!isset(self::$configs[$file])) {
            throw new \RuntimeException("Config file {$file} not found");
        }

        if (!isset(self::$configs[$file][$attribute])) {
            throw new \RuntimeException("{$attribute} not found in {$file} config file");
        }

        return self::$configs[$file][$attribute];
    }

    public static function all($file)
    {
        self::initialize();

        if (!isset(self::$configs[$file])) {
            throw new \RuntimeException("Config file {$file} not found");
        }

        return self::$configs[$file];
    }
}

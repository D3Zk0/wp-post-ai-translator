<?php

namespace D3x\AiTranslator\Requests;

use D3x\AiTranslator\Config;

class AIRequest
{
    protected $API_KEY;
    protected $prompt;
    protected $max_tokens;

    public function __construct($API_KEY)
    {
        $this->API_KEY = $API_KEY;
    }

    public function setPrompt($promptText)
    {
        $this->prompt = $promptText;

    }

    public function setMaxTokens($maxTokens)
    {
        $this->max_tokens = $maxTokens;
    }

    public function call()
    {
        $data = [
            'messages' => [["role" => "system", "content" => $this->prompt]],
//            'max_tokens' => $this->max_tokens,
            'model' => Config::get('ai.MODEL'),
        ];

        $ch = curl_init();

        $options = [
            CURLOPT_URL => Config::get('ai.OPEN_AI_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->API_KEY
            ],
        ];

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);

        if (!$response) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }
        curl_close($ch);

        return json_decode($response);
    }

}
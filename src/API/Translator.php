<?php

namespace D3x\AiTranslator\API;

use D3x\AiTranslator\Config;
use D3x\AiTranslator\Requests\AIRequest;
use http\Exception;

class Translator
{

    const DEFAULT_PROMPT = "Act like translator for multiple languages where you will be translating wordpress posts. I will give you json object of contents with html tags, you give me same json object response where you keep html tags just translate the values of object. Respond only with the json code without any text of what you have done";
    const MAX_TOKENS = 20;

    public static function translate($api_key, $content, $in_language = "slovene", $out_language = "english")
    {
        $prompt = self::DEFAULT_PROMPT . "Translate next content from language locale {$in_language} to language locale {$out_language}: {$content}";
        $JARVIS = new AIRequest($api_key);
        $JARVIS->setPrompt($prompt);
        $JARVIS->setMaxTokens(self::MAX_TOKENS);
        $response = $JARVIS->call();
        $content = self::parse_json($response->choices[0]->message->content);
        $translatedContent = json_decode($content, true);

        // Kliče funkcijo writeToLog, ki zapiše podatke v log datoteko
        self::writeToLog($api_key, $content, $in_language, $out_language, $translatedContent);

        return $translatedContent;
    }

    public static function parse_json($text)
    {
        $firstOpenBracket = strpos($text, "{");
        $lastCloseBracket = strrpos($text, "}");

        // Check if brackets were found
        if ($firstOpenBracket === false || $lastCloseBracket === false) {
            return null; // or handle the error in a way suitable to your application
        }

        // Extract the content between the brackets
        return substr($text, $firstOpenBracket, $lastCloseBracket - $firstOpenBracket + 1);
    }
    public static function writeToLog($api_key, $content, $in_language, $out_language, $translatedContent)
    {
        $logDir = __DIR__ . '/../../logs';
        if (!file_exists($logDir)) {
            if (!mkdir($logDir, 0777, true) && !is_dir($logDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $logDir));
            }
        }
        $filename = $logDir . '/' . date('Y-m-d') . '.txt';
        $logContent = sprintf(
            "API Key: %s\nContent: %s\nInput Language: %s\nOutput Language: %s\nTranslated Content: %s\nExecution Time: %s\n\n#####################################\n\n",
            $api_key,
            $content,
            $in_language,
            $out_language,
            print_r($translatedContent, true),
            date('Y-m-d H:i:s')
        );
        file_put_contents($filename, $logContent, FILE_APPEND);
    }





}
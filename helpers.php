<?php
if (!function_exists('dd')) {
    function dd($data, ...$moreData)
    {
        \Symfony\Component\VarDumper\VarDumper::dump($data);
        foreach ($moreData as $extraData)
            \Symfony\Component\VarDumper\VarDumper::dump($extraData);
        die();
    }
}